#define UNICODE
#define WIN32_LEAN_AND_MEAN
#include <vector>
#include<thread>
#include "Server.h"



std::string exitString = ""; 

int main() {
	
	Server server; 
	server.addRooms();
	std::thread* listenthread = (new std::thread(&Server::acceptClients, server));

	std::cout << "type exit to leave server" << std::endl; 

	
	while (server.sendMessages)
	{
		std::cin >> exitString;
		std::cout << "type exit to leave server" << std::endl;

		if(exitString == "exit")
		server.sendMessages = false;
	}
	

	system("pause");

	
}