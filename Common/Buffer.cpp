#include "Buffer.h"
#include <string>

Buffer::Buffer(size_t size)
	:mWriteIndex(0), mReadIndex(0)
{
	vecBuffer.resize(size);
}

void Buffer::clearBuffer()
{
	vecBuffer.clear();
	vecBuffer.resize(512);
}
void Buffer::WriteInt32LE(size_t index, int value)
{
	vecBuffer[index] = value;
	vecBuffer[index + 1] = value >> 8;
	vecBuffer[index + 2] = value >> 16;
	vecBuffer[index + 3] = value >> 24;

}

void Buffer::WriteShort16BE(size_t index, int value)
{
	vecBuffer[index] = value;
	vecBuffer[index + 1] = value >> 8;


}

void  Buffer::WriteInt32BE(size_t index, int value)
{

	vecBuffer[index] = value >> 24;
	vecBuffer[index + 1] = value >> 16;
	vecBuffer[index + 2] = value  >> 8;
	vecBuffer[index + 3] = value ;

}


int Buffer::ReadInt32LE(size_t index)
{
	uint32_t value = vecBuffer[index];
	value |= vecBuffer[index + 1] << 8;
	value |= vecBuffer[index + 2] << 16;
	value |= vecBuffer[index + 3] << 24;


	return value;
}


int Buffer::ReadInt32BE(size_t index)
{
	uint32_t value = vecBuffer[index] << 24;
	value |= vecBuffer[index + 1] << 16;
	value |= vecBuffer[index + 2] << 8;
	value |= vecBuffer[index + 3];

	return value;
}

short Buffer::ReadShort16BE(size_t index)
{
	uint32_t value = vecBuffer[index] << 8;
	value |= vecBuffer[index + 1];

	return value;
}


void Buffer::WriteString(size_t index, std::string value)
{


	int sizeofstring = value.size();
	int j = 0;
	for (int i = 8; i < sizeofstring+8; i++, j++)
	{
		vecBuffer[i] = value[j];
		int l = 0;
	}


}//end WriteString

std::string Buffer::ReadString(size_t index, int sizeOfReceivedMessage)
{

	std::string value = "";
	
	for (int i = index; i != sizeOfReceivedMessage + index; i++)
	{


		value += vecBuffer[i];
	}

	return value;
}


