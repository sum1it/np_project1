#ifndef _Server_HG_ 
#define _Server_HG_ 

#define UNICODE
#define WIN32_LEAN_AND_MEAN

#include <Windows.h>
#include <WinSock2.h>
#include <WS2tcpip.h>

#include "..\Common\Buffer.h"

#include <iostream>
#include <thread>
#include <sstream>



#define DEFAULT_PORT "5000"
#define DEFAULT_BUFFER_LENGTH 512



enum action
{
	JOIN = 0,
	LEAVE = 1,
	SEND = 2,
	RECEIVE = 3,
	LISTROOMS = 4
};


struct mySocket
{
	SOCKET clientSocket;
	std::string clientName;
	std::vector<std::string>associatedRooms;
	int id;

};

class Server
{
public:

	size_t size;
	size_t index;
	



	Buffer* recvBuffer;
	
	bool sendMessages;
	std::vector<mySocket> clientSockets;
	std::vector<std::thread*> threads;
	std::vector<std::string> chatRooms;

	std::string clientName;

	WSADATA wsaData;
	int iResult;

	char recvbuf[DEFAULT_BUFFER_LENGTH];
	int recvbuflen = DEFAULT_BUFFER_LENGTH;

	SOCKET ListenSocket;
	

	struct addrinfo* result;
	struct addrinfo hints;

	Server();
	~Server(); 
	void acceptClients();												//Accepts Clients on a thread and creates a new thread to listen

	void listenToClient(mySocket tempSocket);							//Keeps on listening for any incoming client

	void sendToClient();												//Send data to a client communicating with the server
			

	void assignToRoom(std::string roomName, mySocket tempSocket);		//Join a Room

	void broadcastMessage(mySocket activeClient, std::string receivedMessage); // broadcast a message to everyone in the current room 

	void leaveFromRoom(mySocket activeClient, std::string roomName);		//Leave a Room

	void listAvailableRooms(mySocket tempSocket);						//Lists all available rooms upon query of the client

	void addRooms();													//Add Rooms to a vector of rooms

};




#endif