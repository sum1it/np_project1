#pragma comment(lib, "Ws2_32.lib")
#include "Server.h"

Server::Server()
{

	size = 512;
	index = 0;
	recvBuffer = new Buffer(size); 
	sendMessages = true;
	recvbuf[DEFAULT_BUFFER_LENGTH];
	recvbuflen = DEFAULT_BUFFER_LENGTH;
	result = 0;

	clientName = "";

	


	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;
	hints.ai_flags = AI_PASSIVE;



	iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != 0) {
		printf("WSAStartup failed: %d\n", iResult);
		return;
	}

	// Socket()
	ListenSocket = socket(AF_INET, SOCK_STREAM, 0);
	if (ListenSocket == INVALID_SOCKET) {
		printf("socket() failed with error %d\n", WSAGetLastError());
		WSACleanup();
		return;
	}

	// Bind()
	iResult = getaddrinfo(NULL, DEFAULT_PORT, &hints, &result);
	iResult = bind(ListenSocket, result->ai_addr, (int)result->ai_addrlen);
	if (iResult == SOCKET_ERROR) {
		printf("bind() failed with error: %d\n", WSAGetLastError());
		freeaddrinfo(result);
		closesocket(ListenSocket);
		WSACleanup();
		return;
	}

	// Listen()
	if (listen(ListenSocket, 5)) {
		printf("listen() failed with error: %d\n", WSAGetLastError());
		closesocket(ListenSocket);
		WSACleanup();
		return;
	}
	return; 
}

Server::~Server()
{
	for (int index = 0; index != threads.size(); index++)
	{
		this->threads[index]->join();
	}

	closesocket(ListenSocket);
	WSACleanup();
	return;
}


void Server::acceptClients()
{
	int clientCounter = 0;
	mySocket tempSocket;

	do {
		SOCKET tempClientSocket;
		// Accept()
		tempClientSocket = accept(ListenSocket, NULL, NULL);
		if (tempClientSocket == INVALID_SOCKET) {
			printf("accept() failed with error: %d\n", WSAGetLastError());
			closesocket(ListenSocket);
			WSACleanup();
		}

		tempSocket.clientSocket = tempClientSocket;
		tempSocket.clientName = "client " + std::to_string(clientCounter);
		tempSocket.id = clientCounter;
		clientCounter++;

		//this->clientSockets.push_back(tempClientSocket);		
		this->clientSockets.push_back(tempSocket);
		std::thread* clientListenThread = (new std::thread(&Server::listenToClient, this, tempSocket));

		this->threads.push_back(clientListenThread);
		std::cout << "Client " << tempSocket.clientName <<" Connected - New Thread Started" << std::endl; 
	} while (sendMessages);

}// end acceptClients
void Server::listenToClient(mySocket tempSocket)
{
	bool connected = true;
	do {

		iResult = recv(tempSocket.clientSocket, recvBuffer->vecBuffer.data(), recvBuffer->vecBuffer.size(), 0);
		if (iResult == SOCKET_ERROR) {
			std::cout << "\nClient " << tempSocket.clientName << " has exited the application" << std::endl;
			closesocket(tempSocket.clientSocket);

			for (int i = 0; i != clientSockets.size(); i++)
			{
				if (clientSockets[i].clientSocket == tempSocket.clientSocket)
				{
					this->clientSockets.erase(clientSockets.begin() + i);
					connected = false;
					break;
				}
			}

		}

		if (iResult > 0) {
			int typeOfProtocol = recvBuffer->ReadInt32BE(0);
			int numOfBytesToRecv = recvBuffer->ReadInt32BE(4);
			std::string receivedMessage = recvBuffer->ReadString(8, numOfBytesToRecv);

			int breakpoint = 0; 


			if (typeOfProtocol == 0)
			{
				listAvailableRooms(tempSocket);
			}
			else if (typeOfProtocol == 1)
			{
				tempSocket.associatedRooms.push_back(receivedMessage);
				std::cout << tempSocket.associatedRooms[0];
				assignToRoom(receivedMessage, tempSocket);
			}
			else if (typeOfProtocol == 2)
			{

				leaveFromRoom(tempSocket, receivedMessage);
			}
			else if(typeOfProtocol == 3)
			{
				broadcastMessage(tempSocket, receivedMessage);
			}
			
		}

	} while (sendMessages && connected);
}// end listenToClient


//void Server::takeAction(std::string message, action type)
//{
//	Buffer tempBuffer(DEFAULT_BUFFER_LENGTH);
//	std::string joinMessage = "has joined the room ";
//	
//	switch (type)
//	{
//	case JOIN:
//		
//		for (int index = 0; index != clientSockets.size(); index++)
//		{
//			tempBuffer.WriteInt32LE(0, strlen(message.c_str()));
//			tempBuffer.WriteInt32LE(strlen(message.c_str()), SEND);
//			iResult = send(clientSockets[index].clientSocket, joinMessage.c_str(), strlen(joinMessage.c_str()), 0);
//			if (iResult == SOCKET_ERROR)
//			{
//				printf("recv failed with error: %d\n", WSAGetLastError());
//				closesocket(clientSockets[index].clientSocket);
//				WSACleanup();
//			}
//
//		}
//		break;
//	case LEAVE:
//		break;
//	case SEND:
//		for (int index = 0; index != clientSockets.size(); index++)
//		{
//			tempBuffer.WriteInt32LE(0, strlen(message.c_str()));
//			tempBuffer.WriteInt32LE(strlen(message.c_str()), SEND);
//			iResult = send(clientSockets[index].clientSocket, tempBuffer.vecBuffer.data(), strlen(tempBuffer.vecBuffer.data()), 0);
//			if (iResult == SOCKET_ERROR) {
//				printf("recv failed with error: %d\n", WSAGetLastError());
//				closesocket(clientSockets[index].clientSocket);
//				WSACleanup();
//
//			}
//
//			tempBuffer.mWriteIndex = 0;
//			tempBuffer.WriteString(strlen(message.c_str()), message);
//			iResult = send(clientSockets[index].clientSocket, tempBuffer.vecBuffer.data(), strlen(tempBuffer.vecBuffer.data()), 0);
//			if (iResult == SOCKET_ERROR) {
//				printf("recv failed with error: %d\n", WSAGetLastError());
//				closesocket(clientSockets[index].clientSocket);
//				WSACleanup();
//
//			}
//		}
//		
//		//tempBuffer.WriteString(strlen(message.c_str()), message);
//		break;
//	case RECEIVE:
//		break;
//
//	case LISTROOMS:
//
//		break;
//	}
//	
//	
//	
//}

void Server::assignToRoom(std::string roomName, mySocket tempSocket)
{
	Buffer tempBuffer(DEFAULT_BUFFER_LENGTH);
	//======================================================================================================================================
	// constructing the join room strings 
	std::string broadcastMessage = "";
	std::string broadcastMessageTOSELF = "";

	broadcastMessage = "**" + tempSocket.clientName + " has joined the room " + tempSocket.associatedRooms.data()->c_str() + "**";

	broadcastMessageTOSELF += "**You have joined the room/s "; // add asterisks and message 
	broadcastMessage = "**" + tempSocket.clientName + " has joined the room/s "; 
	for (int messageIndex = 0; messageIndex != tempSocket.associatedRooms.size(); messageIndex++)
	{
		if (tempSocket.associatedRooms.size() == 1)
		{
			broadcastMessageTOSELF += tempSocket.associatedRooms[messageIndex].data();
			broadcastMessage += tempSocket.associatedRooms[messageIndex].data();
		}
		else
		{
			broadcastMessageTOSELF += tempSocket.associatedRooms[messageIndex].data();
			broadcastMessage += tempSocket.associatedRooms[messageIndex].data();
			if (messageIndex + 1 != tempSocket.associatedRooms.size())
			{
				broadcastMessageTOSELF += " and ";
				broadcastMessage += " and ";
			}
				
		}
	}//end for 
	broadcastMessageTOSELF += "**";// add asterisks to end of message 
	//===============================================================================================================================================
	//TODO
	// sending the join message to everyone in the current room and yourself 

	for (int index = 0; index != clientSockets.size(); index++)
	{
		if (clientSockets[index].clientName == tempSocket.clientName)
		{
			clientSockets[index].associatedRooms.push_back(roomName);
		}// end if 
		for (int roomIndex = 0; roomIndex !=clientSockets[index].associatedRooms.size(); roomIndex++)
		{

			if (clientSockets[index].associatedRooms[roomIndex] == roomName)
			{
				tempBuffer.mWriteIndex = 0;
				if (clientSockets[index].clientSocket == tempSocket.clientSocket)
				{
					tempBuffer.WriteInt32BE(4, broadcastMessageTOSELF.size()); // send message to yourself that you have joined the room 
					tempBuffer.WriteString(strlen(broadcastMessageTOSELF.c_str()), broadcastMessageTOSELF); // send message to yourself that you have joined the room 
				}
				else
				{
					tempBuffer.WriteInt32BE(4, broadcastMessage.size()); // send message to yourself that you have joined the room 
					tempBuffer.WriteString(strlen(broadcastMessage.c_str()), broadcastMessage);// send message to everyone in the room that you have joined 
				}
				iResult = send(clientSockets[index].clientSocket, tempBuffer.vecBuffer.data(), tempBuffer.vecBuffer.size(), 0);
				if (iResult == SOCKET_ERROR) {
					printf("recv failed with error: %d\n", WSAGetLastError());
					closesocket(clientSockets[index].clientSocket);
					WSACleanup();

				}// end if 
			}// end if 
		}// end nested for 
			
	}// end for
	
	
}


void Server::broadcastMessage(mySocket activeClient, std::string receivedMessage)
{
	Buffer tempBuffer(DEFAULT_BUFFER_LENGTH);
	std::vector<std::string> clientsActiveRooms;

	for (int index = 0; index != activeClient.associatedRooms.size(); index++)
	{
		clientsActiveRooms.push_back(activeClient.associatedRooms[index]);
	}// end for 



	for (int index = 0; index != clientSockets.size(); index++)
	{
		if (clientSockets[index].clientSocket == activeClient.clientSocket)
		{
			continue;
		}

		for (int roomIndex = 0; roomIndex != clientsActiveRooms.size(); roomIndex++)
		{
			if (clientSockets[index].associatedRooms.size() == 0)
			{
				continue;
			}

			if (clientSockets[index].associatedRooms[roomIndex] == clientsActiveRooms[roomIndex])
			{
				tempBuffer.WriteInt32BE(4, receivedMessage.size()); // send message to yourself that you have joined the room 
				tempBuffer.WriteString(strlen(receivedMessage.c_str()), receivedMessage); // send message to yourself that you have joined the room 

				iResult = send(clientSockets[index].clientSocket, tempBuffer.vecBuffer.data(), tempBuffer.vecBuffer.size(), 0);
				if (iResult == SOCKET_ERROR) {
					printf("recv failed with error: %d\n", WSAGetLastError());
					closesocket(clientSockets[index].clientSocket);
					WSACleanup();

				}// end if 
			}// end if 
		}// end nested for  
	}// end for 



}// end broadcastMessage








void Server::leaveFromRoom(mySocket activeClient, std::string roomName)
{
	Buffer tempBuffer(DEFAULT_BUFFER_LENGTH);

	std::string leaveMessage = activeClient.clientName + " has left the room " + roomName; 
	std::vector<std::string> clientsActiveRooms;

	for (int index = 0; index != activeClient.associatedRooms.size(); index++)
	{
		clientsActiveRooms.push_back(activeClient.associatedRooms[index]);
	}// end for 



	for (int index = 0; index != clientSockets.size(); index++)
	{
		if (clientSockets[index].clientSocket == activeClient.clientSocket)
		{
			continue;
		}

		for (int roomIndex = 0; roomIndex != clientsActiveRooms.size(); roomIndex++)
		{
			if (clientSockets[index].associatedRooms.size() == 0)
			{
				continue;
			}

			

			if (clientSockets[index].associatedRooms[roomIndex] == clientsActiveRooms[roomIndex])
			{
				tempBuffer.WriteInt32BE(4, leaveMessage.size()); // send message to yourself that you have joined the room 
				tempBuffer.WriteString(strlen(leaveMessage.c_str()), leaveMessage); // send message to yourself that you have joined the room 

				iResult = send(clientSockets[index].clientSocket, tempBuffer.vecBuffer.data(), tempBuffer.vecBuffer.size(), 0);
				if (iResult == SOCKET_ERROR) {
					printf("recv failed with error: %d\n", WSAGetLastError());
					closesocket(clientSockets[index].clientSocket);
					WSACleanup();

				}// end if 
			}// end if 
		}// end nested for  
	}// end for 


	for (int index = 0; index != clientSockets.size(); index++)
	{
		if (clientSockets[index].clientSocket == activeClient.clientSocket)
		{
			for (int roomIndex = 0; roomIndex != clientsActiveRooms.size(); roomIndex++)
			{
				if (roomName == clientsActiveRooms[roomIndex])
				{
					clientSockets[index].associatedRooms.erase(clientSockets[index].associatedRooms.begin() + roomIndex);

				}// end if 
			}// end for 
		}
		
	}//end for 
	
	


}


void Server::listAvailableRooms(mySocket tempSocket)
{
	Buffer tempBuffer(DEFAULT_BUFFER_LENGTH);
	std::string rooms="\n\nAvailable Rooms:\n";
	//TODO
	
	for (int index = 0; index != chatRooms.size(); index++)
	{
		std::cout << chatRooms[index].data() << std::endl;
		rooms = rooms + chatRooms[index].data() + "\n";
	}

	tempBuffer.WriteInt32BE(4, rooms.size());					 // send message to yourself that you have joined the room 
	tempBuffer.WriteString(strlen(rooms.c_str()), rooms);		 // send message to yourself that you have joined the room 


	iResult = send(tempSocket.clientSocket, tempBuffer.vecBuffer.data(), tempBuffer.vecBuffer.size(), 0);
	int breakpoint = 0;
	if (iResult == SOCKET_ERROR)
	{
		printf("recv failed with error: %d\n", WSAGetLastError());
		closesocket(tempSocket.clientSocket);
		WSACleanup();
	}
}

void Server::addRooms()
{

	
	chatRooms.push_back("Networking");
	chatRooms.push_back("Game");
	chatRooms.push_back("Fun");
}

void Server::sendToClient()
{

}