#define UNICODE
#define WIN32_LEAN_AND_MEAN

#include "Client.h"
#include <thread>



int main(int argc, char** argv) {
	if (argc != 2) {
		printf("usage: %s server-name\n", argv[0]);
		return 1;
	}

	Client client;
	
	std::thread* listenthread = (new std::thread(&Client::receiveMessages, client));
	client.sendMessageToServer();


	//void receiveMessages();
	
}