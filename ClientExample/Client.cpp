#include "Client.h"

Client::Client()
{
	ConnectSocket = INVALID_SOCKET;
	clientName = "";

	command = -1;
	sendBuffer = new Buffer(512);
		receiveBuffer = new Buffer(512);
	addrinfo* result = NULL;
	addrinfo* ptr = NULL;

	iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != 0) {
		printf("WSAStartup failed: %d\n", iResult);
		return;
	}

	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;

	iResult = getaddrinfo("127.0.0.1", DEFAULT_PORT, &hints, &result);
	if (iResult != 0) {
		printf("getaddrinfo failed with error: %d\n", iResult);
		WSACleanup();
		return;
	}

	for (ptr = result; ptr != NULL; ptr = ptr->ai_next) {
		ConnectSocket = socket(ptr->ai_family, ptr->ai_socktype, ptr->ai_protocol);
		if (ConnectSocket == INVALID_SOCKET) {
			printf("socket() failed with error: %d\n", iResult);
			WSACleanup();
			return;
		}

		iResult = connect(ConnectSocket, ptr->ai_addr, (int)ptr->ai_addrlen);
		if (iResult == SOCKET_ERROR) {
			closesocket(ConnectSocket);
			ConnectSocket = INVALID_SOCKET;
			continue;
		}

		break;
	}

	freeaddrinfo(result);

	if (ConnectSocket == INVALID_SOCKET) {
		printf("Unable to connect to server");
		WSACleanup();
		return;
	}
}

Client::~Client()
{

	closesocket(ConnectSocket);
	WSACleanup();
}

void Client::sendMessageToServer()
{


	std::string messageTS;
	do
	{
		std::cout << "Type a message : ";							//JOIN FUN
		std::getline(std::cin, input);
		const char* sendbuf = input.c_str();

		std::string firstSendToken = getFirstTextToken(input);		//JOIN 

		if (firstSendToken == "ls" || firstSendToken == "LS") 
		{
			command = LIST;
			messageTS = "ls";
		}
		else if (firstSendToken == "join" || firstSendToken == "JOIN") 
		{
			command = JOIN;
			messageTS = input.substr(input.find_first_of(" \t") + 1);  //FUN
		}
		else if (firstSendToken == "leave" || firstSendToken == "LEAVE") 
		{
			command = LEAVE;
			messageTS = input.substr(input.find_first_of(" \t") + 1);  //FUN
		}		
		else 
		{
			command = MESSAGE;
			messageTS = input;
		}



		sendBuffer->WriteInt32BE(0, command);
		int value = messageTS.size();
		sendBuffer->WriteInt32BE(4, value);		

		sendBuffer->WriteString(8, messageTS);
		int breakpoint = 0;
		

		iResult = send(ConnectSocket, sendBuffer->vecBuffer.data(), sendBuffer->vecBuffer.size(), 0);
		if (iResult == SOCKET_ERROR) {
			printf("socket() failed with error: %d\n", iResult);
			closesocket(ConnectSocket);
			WSACleanup();
			return;
		}
		
		

		sendBuffer->clearBuffer();
	} while (1);

}

std::string Client::getFirstTextToken(std::string clientMessage)
{
	std::string token = clientMessage.substr(0, clientMessage.find(" "));
	return token;
}

void Client::receiveMessages()
{
	bool recvMessage = true;
	bool connected = true;
	do
	{



		iResult = recv(ConnectSocket, receiveBuffer->vecBuffer.data(), receiveBuffer->vecBuffer.size(), 0);
		if (iResult == SOCKET_ERROR) 
		{
			printf("***Server disconnected!***\n");
			closesocket(ConnectSocket);
		}

		if (iResult > 0) 
		{

			int typeOfProtocol = receiveBuffer->ReadInt32BE(0);
			int numOfBytesToRecv = receiveBuffer->ReadInt32BE(4);
			std::string messageRecv = receiveBuffer->ReadString(8, numOfBytesToRecv);

			std::cout << messageRecv << std::endl; 
		}

	} while (recvMessage && connected);
}